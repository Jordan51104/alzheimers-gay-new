import sys
import time
import random
import urwid

def main():
    # read all current notes

    file = open("notes.txt", "r")
    file_read = file.readlines()
    
    i = 0
    
    global notes
    notes = []
    current_note = []
    
    for line in file_read:
        split_line = line.split()
        key = split_line[0]
        note = " ".join(split_line[2:])
        #print key + " " + note
        notes.append(key)
        notes.append(note)
        
    # end reading current notes

    def exit_on_q(key):
        if key in ('Q', 'q'):
            raise urwid.ExitMainLoop()

    txt = urwid.Text("Your Notes: " + "\n" + notes[1] + "\n" + notes[3])
    fill = urwid.Filler(txt, 'top')
    loop = urwid.MainLoop(fill, unhandled_input=exit_on_q)
    loop.run()
    #print "Edit a note\nRead your notes\nExit"
    #print "Enter E to edit your notes\nEnter R to read your notes\nEnter X to exit the application\n"
    #userin = raw_input("What do you want to do?\n")
    #
    #if (userin.capitalize() == "E"):
    #    edit()
    #elif (userin.capitalize() == "R"):
    #    read()
    #elif (userin.capitalize() == "X"):
    #    exit()

def edit():
    x = 1
    file = open("notes.txt", "rw")
    print "File Contents: "
    while x < (len(notes)):
        print notes[x]
        x = x + 2
    print "What do you want to do?"
    print ""
    
    
def read():
    file = open("notes.txt", "r")
    x = 1
    while x < len(notes):
        print notes[x]
        x = x + 2

def exit():
    sys.exit()
    
main()